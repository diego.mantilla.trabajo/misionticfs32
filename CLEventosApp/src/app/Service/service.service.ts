import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Cliente } from '../Modelos/Cliente';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  getClientes(){
    let Url='/rest/cliente/listar';
    let parametros = new HttpParams();

    // Begin assigning parameters
    parametros = parametros.append('nombre', '');
    parametros = parametros.append('page', '1');
    parametros = parametros.append('size', '10');

    return this.http.get<Cliente[]>(Url,{ params:parametros });
  }

  agregarCliente(cliente:Cliente)
  {
    let Url='/rest/cliente/agregar';
    return this.http.post<Cliente>(Url, cliente);
  }

  getClienteId(id:number)
  {
    let Url='/rest/cliente/listar/'+ id;

    return this.http.get<Cliente[]>(Url);
  }

  actualizarCliente(cliente:Cliente)
  {
    let Url='/rest/cliente/editar';
    return this.http.put<Cliente>(Url, cliente);
  }

  eliminarCliente(cliente:Cliente)
  {
    let Url='/rest/cliente/eliminar/'+cliente.idcliente;
    return this.http.delete<Cliente>(Url);
  }

}
