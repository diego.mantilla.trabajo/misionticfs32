import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/Modelos/Cliente';
import { ServiceService } from '../../Service/service.service'

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  clientes:Cliente[];

  constructor(private servicio:ServiceService, private router:Router) { }

  ngOnInit(): void {
    this.servicio.getClientes().subscribe(data=>{ 
      console.log(data);
      this.clientes=data['paginacion']['lista']; 
    });
  }

  Editar(cliente:Cliente)
  {
    localStorage.setItem("id", cliente.idcliente.toString());
    this.router.navigate(["editar"]);
  }

  Eliminar(cliente:Cliente)
  {
    localStorage.setItem("id", cliente.idcliente.toString());
    this.router.navigate(["eliminar"]);
  }

}
