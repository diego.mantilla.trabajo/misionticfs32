import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/Modelos/Cliente';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  cliente: Cliente=new Cliente();

  constructor(private router:Router, private service: ServiceService) { }

  ngOnInit(): void {
    this.Editar();
  }
  
  Editar()
  {
    let id = localStorage.getItem("id");
    this.service.getClienteId(+id)
    .subscribe(data=>{
      this.cliente = data['paginacion']['lista'][0];
    });
  }

  Actualizar(cliente:Cliente)
  {
    this.service.actualizarCliente(cliente)
    .subscribe(data=>{
      alert("Se ha actualizado el registro con exito!!."); 
      this.router.navigate(["listar"]);
    });
  }
}
