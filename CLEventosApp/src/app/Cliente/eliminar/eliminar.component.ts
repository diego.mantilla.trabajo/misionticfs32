import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/Modelos/Cliente';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit {

  cliente: Cliente=new Cliente();

  constructor(private router:Router, private service: ServiceService) { }

  ngOnInit(): void {
    this.CargarRegistro();
  }

  CargarRegistro()
  {
    let id = localStorage.getItem("id");
    this.service.getClienteId(+id)
    .subscribe(data=>{
      this.cliente = data['paginacion']['lista'][0];
    });
  }
  
  Eliminar(cliente:Cliente)
  {
    this.service.eliminarCliente(cliente)
    .subscribe(data=>{
      alert("Se ha eliminado el registro con exito!!."); 
      this.router.navigate(["listar"]);
    });
  }

  Volver()
  {
    this.router.navigate(["listar"]);
  }

}
