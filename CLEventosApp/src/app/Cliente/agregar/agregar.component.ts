import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/Modelos/Cliente';
import { ServiceService } from '../../Service/service.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  cliente: Cliente;

  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.cliente = new Cliente();
  }

  Guardar(cliente:Cliente)
  {
    this.service.agregarCliente(cliente)
    .subscribe(data=>{
      alert("Se ha agregado el registro con exito!!."); 
      this.router.navigate(["listar"]);
    });
  }
}
