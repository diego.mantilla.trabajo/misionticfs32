export class Cliente {
    idcliente: number;
    documento: number;
    tipodoc: string;
    nombres: string;
    apellidos: string;
    telefono: string;
    celular: string;
    email: string;

    constructor()
    {}
}