import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './Cliente/agregar/agregar.component';
import { EditarComponent } from './Cliente/editar/editar.component';
import { EliminarComponent } from './Cliente/eliminar/eliminar.component';
import { ListarComponent } from './Cliente/listar/listar.component';

const routes: Routes = [
  { path:'listar', component: ListarComponent },
  { path:'agregar', component: AgregarComponent },
  { path:'editar', component: EditarComponent},
  { path:'eliminar', component: EliminarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
