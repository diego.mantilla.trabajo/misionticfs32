/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.utilitario;

import java.util.HashMap;

/**
 *
 * @author JCode
 */
public class ParametrosPorDefecto {

    public static HashMap<String, Object> getParametrosServicio() {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("FILTER", "");
        parametros.put("SQL_ORDER_BY", " ORDER BY servicio ASC");
        parametros.put("SQL_PAGINATION", "LIMIT 10 OFFSET 0");
        return parametros;
    }

    public static HashMap<String, Object> getParametrosCliente() {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("FILTER", "");
        parametros.put("SQL_ORDER_BY", " ORDER BY documento ASC");
        parametros.put("SQL_PAGINATION", "LIMIT 10 OFFSET 0");
        return parametros;
    }

    public static HashMap<String, Object> getParametrosReserva() {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("FILTER", "");
        parametros.put("SQL_ORDER_BY", " ORDER BY fecha ASC");
        parametros.put("SQL_PAGINATION", "LIMIT 10 OFFSET 0");
        return parametros;
    }

    public static HashMap<String, Object> getParametrosCurso() {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("FILTER", "");
        parametros.put("SQL_ORDER_BY", " ORDER BY curso ASC");
        parametros.put("SQL_PAGINATION", "LIMIT 10 OFFSET 0");
        return parametros;
    }
    
        public static HashMap<String, Object> getParametrosSalon() {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("FILTER", "");
        parametros.put("SQL_ORDER_BY", " ORDER BY salon ASC");
        parametros.put("SQL_PAGINATION", "LIMIT 10 OFFSET 0");
        return parametros;
    }
}
