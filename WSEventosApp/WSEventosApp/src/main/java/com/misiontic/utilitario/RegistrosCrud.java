/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.utilitario;

/**
 *
 * @author DIEGO-PC
 */
public class RegistrosCrud {
    private String mensaje_servidor;
    private Paginacion paginacion;

    public RegistrosCrud() {
    }

    
    public RegistrosCrud(Paginacion paginacion) {
        this.paginacion = paginacion;
    }

    public String getMensaje_servidor() {
        return mensaje_servidor;
    }

    public void setMensaje_servidor(String mensaje_servidor) {
        this.mensaje_servidor = mensaje_servidor;
    }

    public Paginacion getPaginacion() {
        return paginacion;
    }

    public void setPaginacion(Paginacion paginacion) {
        this.paginacion = paginacion;
    }
    
    
    
}
