/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.utilitario;

import java.util.List;

/**
 *
 * @author DIEGO-PC
 */
public class Paginacion {
    private Integer filtro_contador;
    private List<?> lista;

    public Paginacion() {
        
    }

    public Paginacion(Integer filtro_contador, List<?> lista) {
        this.filtro_contador = filtro_contador;
        this.lista = lista;
    }

    public Integer getFiltro_contador() {
        return filtro_contador;
    }

    public void setFiltro_contador(Integer filtro_contador) {
        this.filtro_contador = filtro_contador;
    }

    public List<?> getLista() {
        return lista;
    }

    public void setLista(List<?> lista) {
        this.lista = lista;
    }
    
    
}
