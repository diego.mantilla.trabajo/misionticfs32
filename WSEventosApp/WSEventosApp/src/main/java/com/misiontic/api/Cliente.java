/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.api;

import com.misiontic.dao.ClienteDAO;
import com.misiontic.dao.imp.ClienteDAOImp;
import com.misiontic.utilitario.ParametrosPorDefecto;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author DIEGO-PC
 */
@Singleton
@Path("/cliente")
public class Cliente {

    private static final Logger LOG = Logger.getLogger(Cliente.class.getName());
    private final String lookup = "java:/comp/env/";
    private DataSource pool;
    private ClienteDAO clienteDAO;

    public Cliente() {
        try {
            InitialContext cxt = new InitialContext();
            this.pool = (DataSource) cxt.lookup(lookup + "jdbc/eventosapp");
            if (this.pool != null) {
                LOG.info("Data Source Inicializado exitosamente!");
            } else {
                LOG.info("Error al Inicializar DataSource");
            }
            this.clienteDAO = new ClienteDAOImp(this.pool);
        } catch (NamingException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Path("/listar")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response paginate(
            @QueryParam("nombre") String nombre,
            @QueryParam("page") Integer page,
            @QueryParam("size") Integer size) throws SQLException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("FILTER", nombre == null? "" : nombre);
        parameters.put("SQL_ORDER_BY", " ORDER BY apellidos ASC");
        parameters.put("SQL_PAGINATION", page == null? "" : " LIMIT " + size + " OFFSET "
                + (page - 1) * size);
        LOG.info(parameters.toString());
        return Response.status(Response.Status.OK).entity(this.clienteDAO.getLista(parameters)).build();
    }

    @Path("/agregar")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(com.misiontic.modelo.Cliente cliente) throws SQLException {
        LOG.info(cliente.toString());
        return Response.status(Response.Status.OK).entity(this.clienteDAO.agregar(cliente, ParametrosPorDefecto.getParametrosCliente())).build();
    }

    @Path("/editar")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response udpate(com.misiontic.modelo.Cliente cliente) throws SQLException {
        LOG.info(cliente.toString());
        return Response.status(Response.Status.OK).entity(this.clienteDAO.editar(cliente, ParametrosPorDefecto.getParametrosCliente())).build();
    }

    @Path("/eliminar/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) throws SQLException {
        LOG.info(id.toString());
        return Response.status(Response.Status.OK).entity(this.clienteDAO.eliminar(id, ParametrosPorDefecto.getParametrosCliente())).build();
    }

    @Path("/listar/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response paginate(@PathParam("id") Integer id) throws SQLException {
        LOG.info(id.toString());
        return Response.status(Response.Status.OK).entity(this.clienteDAO.getLista(id)).build();
    }

    
}