/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.api;

import com.misiontic.dao.ClienteDAO;
import com.misiontic.dao.imp.ClienteDAOImp;
import com.misiontic.utilitario.ParametrosPorDefecto;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author DIEGO-PC
 */
@Singleton
@Path("/ejemplo")
public class Ejemplo {

    private static final Logger LOG = Logger.getLogger(Cliente.class.getName());
    private final String lookup = "java:/comp/env/";
    private DataSource pool;
    private ClienteDAO clienteDAO;

    public Ejemplo() {
        try {
            InitialContext cxt = new InitialContext();
            this.pool = (DataSource) cxt.lookup(lookup + "jdbc/eventosapp");
            if (this.pool != null) {
                LOG.info("Data Source Inicializado exitosamente!");
            } else {
                LOG.info("Error al Inicializar DataSource");
            }
            this.clienteDAO = new ClienteDAOImp(this.pool);
        } catch (NamingException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Path("/listar")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String paginate() throws SQLException {
        
        return "Se consumio el metodo listar";
    }


}