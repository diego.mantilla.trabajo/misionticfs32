/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author DIEGO-PC
 */
@Path("/saludo")
public class HolaMundo {
    
    @Path("/saludar")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getSaludo()
    {
        return "Hola Amigos";
    }
}
