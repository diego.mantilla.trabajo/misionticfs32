/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

/**
 *
 * @author DIEGO-PC
 */
public class Salon {
    private int idSalon;
    private String salon;
    private String descripcion;
    private String direccion;
    private double valorHora;

    public Salon() {
    }

    public Salon(int idSalon, String salon, String descripcion, String direccion, double valorHora) {
        this.idSalon = idSalon;
        this.salon = salon;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.valorHora = valorHora;
    }

    public int getIdSalon() {
        return idSalon;
    }

    public void setIdSalon(int idSalon) {
        this.idSalon = idSalon;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getValorHora() {
        return valorHora;
    }

    public void setValorHora(double valorHora) {
        this.valorHora = valorHora;
    }
    
    
}
