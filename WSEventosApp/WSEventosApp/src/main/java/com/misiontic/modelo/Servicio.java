/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

/**
 *
 * @author DIEGO-PC
 */
public class Servicio {
    private Integer idservicio;
    private String servicio;
    private double vr_servicio;

    public Servicio() {
    }

    public Servicio(Integer idservicio, String servicio, double vr_servicio) {
        this.idservicio = idservicio;
        this.servicio = servicio;
        this.vr_servicio = vr_servicio;
    }

    public Integer getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public double getVr_servicio() {
        return vr_servicio;
    }

    public void setVr_servicio(double vr_servicio) {
        this.vr_servicio = vr_servicio;
    }
    
    @Override
    public String toString()
    {
        return "Servicio{idservicio=" + idservicio + ", servicio=" + servicio + ", vr_servicio=" + String.valueOf(vr_servicio) + "}";
    }
}
