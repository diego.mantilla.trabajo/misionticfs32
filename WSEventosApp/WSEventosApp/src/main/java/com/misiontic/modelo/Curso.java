/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.modelo;

/**
 *
 * @author DIEGO-PC
 */
public class Curso {
    private int idCurso;
    private String curso;

    public Curso() {
    }

    public Curso(int idCurso, String curso) {
        this.idCurso = idCurso;
        this.curso = curso;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    
}
