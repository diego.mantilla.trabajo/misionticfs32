package com.misiontic.dao.imp;

import com.misiontic.dao.ClienteDAO;
import com.misiontic.dao.ServicioDAO;
import com.misiontic.dao.SqlCierre;
import com.misiontic.modelo.Cliente;
import com.misiontic.modelo.Servicio;
import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DIEGO-PC
 */
public class ClienteDAOImp implements ClienteDAO {

private static final Logger LOG = Logger.getLogger(ClienteDAOImp.class.getName());
    private final DataSource pool;

    public ClienteDAOImp(DataSource pool) {
        this.pool = pool;
    }

    @Override
    public Paginacion getLista(HashMap<String, Object> parameters, Connection conn) throws SQLException {
        Paginacion paginacion = new Paginacion();
        List<Cliente> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = conn.prepareStatement("SELECT COUNT(idcliente) AS COUNT FROM evp_cliente WHERE "
                    + "documento LIKE CONCAT('%',?,'%')");
            pst.setString(1, String.valueOf(parameters.get("FILTER")));
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                paginacion.setFiltro_contador(rs.getInt("COUNT"));
                if (rs.getInt("COUNT") > 0) {
                    pst = conn.prepareStatement("SELECT * FROM evp_cliente WHERE documento LIKE CONCAT('%',?,'%') "
                            + String.valueOf(parameters.get("SQL_ORDER_BY")) + " " + String.valueOf(parameters.get("SQL_PAGINATION")));
                    pst.setString(1, String.valueOf(parameters.get("FILTER")));
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        Cliente registro = new Cliente();
                        registro.setIdcliente(rs.getInt("idcliente"));
                        registro.setDocumento(rs.getString("documento"));
                        registro.setTipodoc(rs.getString("tipodoc"));
                        registro.setNombres(rs.getString("nombres"));
                        registro.setApellidos(rs.getString("apellidos"));
                        registro.setTelefono(rs.getString("telefono"));
                        registro.setCelular(rs.getString("celular"));
                        registro.setEmail(rs.getString("email"));
                        list.add(registro);
                    }
                }
            }
            paginacion.setLista(list);
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return paginacion;
    }

    @Override
    public RegistrosCrud getLista(Integer id) throws SQLException {
        RegistrosCrud resultado = new RegistrosCrud();
        Paginacion paginacion = new Paginacion();
        List<Cliente> list = new ArrayList<>();
        Cliente cl = this.getForId(Long.valueOf(String.valueOf(id))); 
        list.add(cl);
        paginacion.setLista(list);
        resultado.setPaginacion(paginacion);

        return resultado;
    }

    @Override
    public RegistrosCrud getLista(HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection()) {
            crud.setPaginacion(getLista(parameters, conn));
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud agregar(Cliente t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idcliente) AS COUNT FROM evp_cliente WHERE documento = ?");
            pst.setString(1, t.getDocumento());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("INSERT INTO evp_cliente(documento, tipodoc, nombres, apellidos, telefono, celular, email) VALUES(?,?,?,?,?,?,?)");
                    pst.setString(1, t.getDocumento());
                    pst.setString(2, t.getTipodoc());
                    pst.setString(3, t.getNombres());
                    pst.setString(4, t.getApellidos());
                    pst.setString(5, t.getTelefono());
                    pst.setString(6, t.getCelular());
                    pst.setString(7, t.getEmail());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se registró, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud editar(Cliente t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idcliente) AS COUNT FROM evp_cliente WHERE documento = ? AND idcliente != ?");
            pst.setString(1, t.getDocumento());
            pst.setInt(2, t.getIdcliente());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("UPDATE evp_cliente SET documento = ?, tipodoc = ?, nombres = ?, apellidos = ?, telefono = ?, celular = ?, email = ? WHERE idcliente = ?");
                    pst.setString(1, t.getDocumento());
                    pst.setString(2, t.getTipodoc());
                    pst.setString(3, t.getNombres());
                    pst.setString(4, t.getApellidos());
                    pst.setString(5, t.getTelefono());
                    pst.setString(6, t.getCelular());
                    pst.setString(7, t.getEmail());
                    pst.setInt(8, t.getIdcliente());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se modificó, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud eliminar(Integer id, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud beanCrud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            try (PreparedStatement pst = conn.prepareStatement("DELETE FROM evp_cliente WHERE idcliente = ?")) {
                pst.setInt(1, id);
                LOG.info(pst.toString());
                pst.executeUpdate();
                conn.commit();
                beanCrud.setMensaje_servidor("ok");
                beanCrud.setPaginacion(getLista(parameters, conn));
            }
        } catch (SQLException e) {
            throw e;
        }
        return beanCrud;
    }

    @Override
    public Cliente getForId(Long id) {
        PreparedStatement pst;
        ResultSet rs;
        Cliente registro = null;
        try {
            //Cargar la información del cliente
                    Connection conn = this.pool.getConnection();
                    pst = conn.prepareStatement("SELECT idcliente, documento, tipodoc, nombres, apellidos, telefono, celular, email FROM evp_cliente WHERE idcliente = ?");
                    pst.setLong(1, id);
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        registro = new Cliente();
                        registro.setIdcliente(rs.getInt("idcliente"));
                        registro.setDocumento(rs.getString("documento"));
                        registro.setTipodoc(rs.getString("tipodoc"));
                        registro.setNombres(rs.getString("nombres"));
                        registro.setApellidos(rs.getString("apellidos"));
                        registro.setTelefono(rs.getString("telefono"));
                        registro.setCelular(rs.getString("celular"));
                        registro.setEmail(rs.getString("email"));
                    }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return registro;
    }
    
}
