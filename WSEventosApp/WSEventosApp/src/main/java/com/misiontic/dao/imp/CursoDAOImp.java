package com.misiontic.dao.imp;

import com.misiontic.dao.CursoDAO;
import com.misiontic.dao.SqlCierre;
import com.misiontic.modelo.Curso;
import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DIEGO-PC
 */
public class CursoDAOImp implements CursoDAO {

private static final Logger LOG = Logger.getLogger(CursoDAOImp.class.getName());
    private final DataSource pool;

    public CursoDAOImp(DataSource pool) {
        this.pool = pool;
    }

    @Override
    public Paginacion getLista(HashMap<String, Object> parameters, Connection conn) throws SQLException {
        Paginacion paginacion = new Paginacion();
        List<Curso> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = conn.prepareStatement("SELECT COUNT(idCurso) AS COUNT FROM cursos WHERE "
                    + "curso LIKE CONCAT('%',?,'%')");
            pst.setString(1, String.valueOf(parameters.get("FILTER")));
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                paginacion.setFiltro_contador(rs.getInt("COUNT"));
                if (rs.getInt("COUNT") > 0) {
                    pst = conn.prepareStatement("SELECT idCurso, curso FROM cursos WHERE curso LIKE CONCAT('%',?,'%') "
                            + String.valueOf(parameters.get("SQL_ORDER_BY")) + " " + String.valueOf(parameters.get("SQL_PAGINATION")));
                    pst.setString(1, String.valueOf(parameters.get("FILTER")));
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        Curso registro = new Curso();
                        registro.setIdCurso(rs.getInt("idCurso"));
                        registro.setCurso(rs.getString("curso"));
                        list.add(registro);
                    }
                }
            }
            paginacion.setLista(list);
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return paginacion;
    }

    @Override
    public RegistrosCrud getLista(HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection()) {
            crud.setPaginacion(getLista(parameters, conn));
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud agregar(Curso t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idCurso) AS COUNT FROM cursos WHERE curso = ?");
            pst.setString(1, t.getCurso());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("INSERT INTO cursos(curso) VALUES(?)");
                    pst.setString(1, t.getCurso());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se registró, ya existe un Curso con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud editar(Curso t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idCurso) AS COUNT FROM cursos WHERE curso = ? AND idCurso != ?");
            pst.setString(1, t.getCurso());
            pst.setInt(2, t.getIdCurso());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("UPDATE cursos SET curso = ? WHERE idCurso = ?");
                    pst.setString(1, t.getCurso());
                    pst.setInt(2, t.getIdCurso());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se modificó, ya existe un Curso con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud eliminar(Integer id, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud beanCrud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            try (PreparedStatement pst = conn.prepareStatement("DELETE FROM cursos WHERE idCurso = ?")) {
                pst.setInt(1, id);
                LOG.info(pst.toString());
                pst.executeUpdate();
                conn.commit();
                beanCrud.setMensaje_servidor("ok");
                beanCrud.setPaginacion(getLista(parameters, conn));
            }
        } catch (SQLException e) {
            throw e;
        }
        return beanCrud;
    }

    @Override
    public Curso getForId(Long id) {
        PreparedStatement pst;
        ResultSet rs;
        Curso registro = null;
        try {
                    Connection conn = this.pool.getConnection();
                    pst = conn.prepareStatement("SELECT idCurso, curso FROM cursos WHERE idCurso = ?");
                    pst.setLong(1, id);
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        registro = new Curso();
                        registro.setIdCurso(rs.getInt("idCurso"));
                        registro.setCurso(rs.getString("curso"));
                    }
            
            rs.close();
            pst.close();
        } catch (SQLException e) {
        }
        return registro;
    }

    @Override
    public RegistrosCrud getLista(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
