package com.misiontic.dao.imp;

import com.misiontic.dao.ServicioDAO;
import com.misiontic.dao.SqlCierre;
import com.misiontic.modelo.Servicio;
import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DIEGO-PC
 */
public class ServicioDAOImp implements ServicioDAO {

private static final Logger LOG = Logger.getLogger(ServicioDAOImp.class.getName());
    private final DataSource pool;

    public ServicioDAOImp(DataSource pool) {
        this.pool = pool;
    }

    @Override
    public Paginacion getLista(HashMap<String, Object> parameters, Connection conn) throws SQLException {
        Paginacion paginacion = new Paginacion();
        List<Servicio> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = conn.prepareStatement("SELECT COUNT(idservicio) AS COUNT FROM evp_servicio WHERE "
                    + "servicio LIKE CONCAT('%',?,'%')");
            pst.setString(1, String.valueOf(parameters.get("FILTER")));
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                paginacion.setFiltro_contador(rs.getInt("COUNT"));
                if (rs.getInt("COUNT") > 0) {
                    pst = conn.prepareStatement("SELECT * FROM evp_servicio WHERE servicio LIKE CONCAT('%',?,'%') "
                            + String.valueOf(parameters.get("SQL_ORDER_BY")) + " " + String.valueOf(parameters.get("SQL_PAGINATION")));
                    pst.setString(1, String.valueOf(parameters.get("FILTER")));
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        Servicio registro = new Servicio();
                        registro.setIdservicio(rs.getInt("idservicio"));
                        registro.setServicio(rs.getString("servicio"));
                        registro.setVr_servicio(rs.getDouble("vr_servicio"));
                        list.add(registro);
                    }
                }
            }
            paginacion.setLista(list);
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return paginacion;
    }

    @Override
    public RegistrosCrud getLista(HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection()) {
            crud.setPaginacion(getLista(parameters, conn));
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud agregar(Servicio t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idservicio) AS COUNT FROM evp_servicio WHERE servicio = ?");
            pst.setString(1, t.getServicio());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("INSERT INTO evp_servicio(servicio, vr_servicio) VALUES(?,?)");
                    pst.setString(1, t.getServicio());
                    pst.setDouble(2, t.getVr_servicio());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se registró, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud editar(Servicio t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idservicio) AS COUNT FROM evp_servicio WHERE servicio = ? AND idservicio != ?");
            pst.setString(1, t.getServicio());
            pst.setInt(2, t.getIdservicio());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("UPDATE evp_servicio SET servicio = ?, vr_servicio = ? WHERE idservicio = ?");
                    pst.setString(1, t.getServicio());
                    pst.setDouble(2, t.getVr_servicio());
                    pst.setInt(3, t.getIdservicio());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se modificó, ya existe un Servicio con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud eliminar(Integer id, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud beanCrud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            try (PreparedStatement pst = conn.prepareStatement("DELETE FROM evp_servicio WHERE idservicio = ?")) {
                pst.setInt(1, id);
                LOG.info(pst.toString());
                pst.executeUpdate();
                conn.commit();
                beanCrud.setMensaje_servidor("ok");
                beanCrud.setPaginacion(getLista(parameters, conn));
            }
        } catch (SQLException e) {
            throw e;
        }
        return beanCrud;
    }

    @Override
    public Servicio getForId(Long id) {
        PreparedStatement pst;
        ResultSet rs;
        Servicio registro = null;
        try {
                    Connection conn = this.pool.getConnection();
                    pst = conn.prepareStatement("SELECT idservicio, servicio, vr_servicio FROM evp_servicio WHERE idservicio = ?");
                    pst.setLong(1, id);
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        registro = new Servicio();
                        registro.setIdservicio(rs.getInt("idservicio"));
                        registro.setServicio(rs.getString("servicio"));
                        registro.setVr_servicio(rs.getDouble("vr_servicio"));
                    }
            rs.close();
            pst.close();
        } catch (SQLException e) {
        }
        return registro;
    }

    @Override
    public RegistrosCrud getLista(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
