/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.dao;

import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author DIEGO-PC
 * @param <T>
 */
public interface CRUD<T> {
    Paginacion getLista(HashMap<String, Object> parametros, Connection conexion) throws SQLException;
    RegistrosCrud getLista(HashMap<String, Object> parametros) throws SQLException;
    RegistrosCrud getLista(Integer id) throws SQLException;
    RegistrosCrud agregar(T t, HashMap<String, Object> parametros) throws SQLException;
    RegistrosCrud editar(T t, HashMap<String, Object> parametros) throws SQLException;
    RegistrosCrud eliminar(Integer id, HashMap<String, Object> parametros) throws SQLException;
    T getForId(Long id);
}
