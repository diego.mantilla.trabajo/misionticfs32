package com.misiontic.dao.imp;

import com.misiontic.dao.ClienteDAO;
import com.misiontic.dao.SalonDAO;
import com.misiontic.dao.ServicioDAO;
import com.misiontic.dao.SqlCierre;
import com.misiontic.modelo.Cliente;
import com.misiontic.modelo.Salon;
import com.misiontic.modelo.Servicio;
import com.misiontic.utilitario.Paginacion;
import com.misiontic.utilitario.RegistrosCrud;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DIEGO-PC
 */
public class SalonDAOImp implements SalonDAO {

private static final Logger LOG = Logger.getLogger(SalonDAOImp.class.getName());
    private final DataSource pool;

    public SalonDAOImp(DataSource pool) {
        this.pool = pool;
    }

    @Override
    public Paginacion getLista(HashMap<String, Object> parameters, Connection conn) throws SQLException {
        Paginacion paginacion = new Paginacion();
        List<Salon> list = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = conn.prepareStatement("SELECT COUNT(idsalon) AS COUNT FROM evp_salon WHERE "
                    + "salon LIKE CONCAT('%',?,'%')");
            pst.setString(1, String.valueOf(parameters.get("FILTER")));
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                paginacion.setFiltro_contador(rs.getInt("COUNT"));
                if (rs.getInt("COUNT") > 0) {
                    pst = conn.prepareStatement("SELECT * FROM evp_salon WHERE salon LIKE CONCAT('%',?,'%') "
                            + String.valueOf(parameters.get("SQL_ORDER_BY")) + " " + String.valueOf(parameters.get("SQL_PAGINATION")));
                    pst.setString(1, String.valueOf(parameters.get("FILTER")));
                    LOG.info(pst.toString());
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        Salon registro = new Salon();
                        registro.setIdSalon(rs.getInt("idsalon"));
                        registro.setSalon(rs.getString("salon"));
                        registro.setDescripcion(rs.getString("descripcion"));
                        registro.setDireccion(rs.getString("direccion"));
                        registro.setValorHora(rs.getDouble("valor_hora"));
                        list.add(registro);
                    }
                }
            }
            paginacion.setLista(list);
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return paginacion;
    }

    @Override
    public RegistrosCrud getLista(Integer id) throws SQLException {
        RegistrosCrud resultado = new RegistrosCrud();
        Paginacion paginacion = new Paginacion();
        List<Salon> list = new ArrayList<>();
        Salon cl = this.getForId(Long.valueOf(String.valueOf(id))); 
        list.add(cl);
        paginacion.setLista(list);
        resultado.setPaginacion(paginacion);

        return resultado;
    }

    @Override
    public RegistrosCrud getLista(HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection()) {
            crud.setPaginacion(getLista(parameters, conn));
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud agregar(Salon t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idsalon) AS COUNT FROM evp_salon WHERE salon = ?");
            pst.setString(1, t.getSalon());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("INSERT INTO evp_salon (salon, descripcion, direccion, valor_hora) VALUES (?, ?, ?, ?)");
                    pst.setString(1, t.getSalon());
                    pst.setString(2, t.getDescripcion());
                    pst.setString(3, t.getDireccion());
                    pst.setDouble(4, t.getValorHora());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se registró, ya existe un Salon con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud editar(Salon t, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud crud = new RegistrosCrud();
        PreparedStatement pst;
        ResultSet rs;
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            pst = conn.prepareStatement("SELECT COUNT(idsalon) AS COUNT FROM evp_salon WHERE salon = ? AND idsalon != ?");
            pst.setString(1, t.getSalon());
            pst.setInt(2, t.getIdSalon());
            LOG.info(pst.toString());
            rs = pst.executeQuery();
            while (rs.next()) {
                if (rs.getInt("COUNT") == 0) {
                    //REALIZAMOS LA TRANSACCIÓN
                    pst = conn.prepareStatement("UPDATE evp_salon SET salon = ?, descripcion = ?, direccion = ?, valor_hora = ? WHERE (idsalon = ?)");
                    pst.setString(1, t.getSalon());
                    pst.setString(2, t.getDescripcion());
                    pst.setString(3, t.getDireccion());
                    pst.setDouble(4, t.getValorHora());
                    pst.setInt(5, t.getIdSalon());
                    LOG.info(pst.toString());
                    pst.executeUpdate();
                    conn.commit();
                    crud.setMensaje_servidor("ok");
                    crud.setPaginacion(getLista(parameters, conn));
                } else {
                    //RECHAZAMOS EL REGISTRO
                    crud.setMensaje_servidor("No se encontró un Salon con el nombre ingresado");
                }
            }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            throw e;
        }
        return crud;
    }

    @Override
    public RegistrosCrud eliminar(Integer id, HashMap<String, Object> parameters) throws SQLException {
        RegistrosCrud beanCrud = new RegistrosCrud();
        try (Connection conn = this.pool.getConnection();
                SqlCierre finish = conn::rollback;) {
            conn.setAutoCommit(false);
            try (PreparedStatement pst = conn.prepareStatement("DELETE FROM evp_salon WHERE idsalon = ?")) {
                pst.setInt(1, id);
                LOG.info(pst.toString());
                pst.executeUpdate();
                conn.commit();
                beanCrud.setMensaje_servidor("ok");
                beanCrud.setPaginacion(getLista(parameters, conn));
            }
        } catch (SQLException e) {
            throw e;
        }
        return beanCrud;
    }

    @Override
    public Salon getForId(Long id) {
        PreparedStatement pst;
        ResultSet rs;
        Salon registro = null;
        try {
            //Cargar la información del cliente
                    Connection conn = this.pool.getConnection();
                    pst = conn.prepareStatement("SELECT idsalon, salon, descripcion, direccion, valor_hora FROM evp_salon WHERE idsalon = ?");
                    pst.setLong(1, id);
                    rs = pst.executeQuery();
                    while (rs.next()) {
                        registro = new Salon();
                        registro.setIdSalon(rs.getInt("idsalon"));
                        registro.setSalon(rs.getString("salon"));
                        registro.setDescripcion(rs.getString("descripcion"));
                        registro.setDireccion(rs.getString("direccion"));
                        registro.setValorHora(rs.getDouble("valor_hora"));
                    }
            rs.close();
            pst.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return registro;
    }
    
}
